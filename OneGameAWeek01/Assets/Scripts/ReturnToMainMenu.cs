using UnityEngine;
using UnityEngine.SceneManagement;

namespace Minigame
{
    public class ReturnToMainMenu : MonoBehaviour
    {
        /// <summary>
        /// Serialized reference to the HighScore, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private HighScore highScore;

        /// <summary>
        /// Serialized string value, name of the MainMenu Scene, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private string mainMenuSceneName;

        private void Update()
        {
            // We check if the Escape-Key on the keyboard was hit
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // If the key was hit we call the SaveHighScore() method of our highScore
                highScore.SaveHighScore();

                // After the save operation we load our MainMenu Scene by its name
                SceneManager.LoadScene(mainMenuSceneName);
            }
        }
    }
}