using UnityEngine;

public class SpriteColorChanger : MonoBehaviour
{
    /// <summary>
    /// Public Health variable we adjust in the Inspector
    /// </summary>
    public  int            health = 100;
    
    /// <summary>
    /// Public Color variable we set in the Inspector
    /// </summary>
    public  Color          deathColor;
    
    /// <summary>
    /// Private reference to our attached SpriteRenderer
    /// </summary>
    private SpriteRenderer megamanRenderer;
    
    /// <summary>
    /// Cached variable of the color at startup
    /// </summary>
    private Color          initialColor;
    
    private void Start()
    {
        // With GetComponent we get any component of the gameObject, in this case the SpriteRenderer
        megamanRenderer = GetComponent<SpriteRenderer>();
        
        // We store the initial color of our spriteRenderer to use it later
        initialColor    = megamanRenderer.color;
    }

    private void Update()
    {
        // If health is below 50, we set the color to the deathColor variable
        if (health < 50)
        {
            megamanRenderer.color = deathColor;
        }
        // If health is above 50 we set it to the value of the initialColor variable, where we 
        // stored the initial value of the color of the spriteRenderer
        else
        {
            megamanRenderer.color = initialColor;
        }
    }
}