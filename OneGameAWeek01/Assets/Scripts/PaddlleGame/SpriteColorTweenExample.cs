using DG.Tweening;
using UnityEngine;

public class SpriteColorTweenExample : MonoBehaviour
{
    /// <summary>
    /// Public variable for the reference of a SpriteRenderer
    /// </summary>
    public SpriteRenderer spriteRenderer;

    /// <summary>
    /// Public variable for the desired Color to tween to
    /// </summary>
    public Color newSpriteColor;

    /// <summary>
    /// Public variable for the desired duration of the tween
    /// </summary>
    public float tweenDuration;

    /// <summary>
    /// Public Method we hook up in the OnClick-Handler of a Unity UI Button
    /// </summary>
    public void PlaySpriteTween()
    {
        // With the .DOColor DOTween method we can tween the color of a sprite
        // to a desired value with our parameters
        spriteRenderer.DOColor(newSpriteColor, tweenDuration);
    }
}