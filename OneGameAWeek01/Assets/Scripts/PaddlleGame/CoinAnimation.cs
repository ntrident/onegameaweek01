using System;
using UnityEngine;
public class CoinAnimation : MonoBehaviour
{
    /// <summary>
    /// Serialized reference to the Animator Component
    /// </summary>
    [SerializeField]
    private Animator coinAnimator;

    /// <summary>
    /// Serialized value for the state of the Animator we want to play
    /// </summary>
    [SerializeField]
    private string stateName;
    
    private void Start()
    {
        // With the .Play() Method of the Animator component we tell the Animator
        // exactly which state to play. We pass the stateName variable as the argument
        // for the state to play.
        // See the Unity documentation for further information of the Play Method
        coinAnimator.Play(stateName);
    }
}