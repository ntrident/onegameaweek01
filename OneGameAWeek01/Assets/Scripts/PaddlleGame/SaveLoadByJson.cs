using System.IO;
using UnityEngine;

public class SaveLoadByJson : MonoBehaviour
{
    /// <summary>
    /// A private variable where we store an instance of the Player component
    /// </summary>
    private Player player;
    
    /// <summary>
    /// A private variable where we store the actual filePath we construct for saving our PlayerData
    /// </summary>
    private string filePath;

    private void Awake()
    {
        // The string property .persistentDataPath in the Application class of Unity points
        // to a very specific folder, a directory path where you can store data that you want to be kept between runs
        // It's a different location for each platform, please lookup the Scripting Manual
        Debug.Log("Location of Datapath: " + Application.persistentDataPath);
        
        // Since we create a file or overwrite an existing file when saving we have to define the filePath
        // We combine the persistentDataPath, effectively the folder where the savefile will be, with a custom file, consisting
        // of a name and the filetype - since we will use the JsonUtility for saving and loading, it's wise to set .json as the
        // filetype
        filePath = Application.persistentDataPath + "/PlayerData.json";
        
        // We load the PlayerData at startup
        LoadFromJson();
    }

    public void SaveToJson()
    {
        // Since we want to save the playerData residing in the Player Component, we need access to it
        // We find the gameObject by Tag and retrieve the Component
        player = GameObject.FindWithTag(GameTags.PLAYER).GetComponent<Player>();

        // We access the playerData via the property (.Data) and translate it with the .ToJson method
        // into the json text format and assign it a string which will hold this formatted json text
        string playerDataToSave = JsonUtility.ToJson(player.Data);
        
        // With the System.IO class File we can utilize the .WriteAllText method in order to actually
        // create a new text file at the specified filePath and populate that file with specific contents, in
        // our case the playerData formatted as a json text stored in the local string variable
        // If a file with the same filename exists at that path we overwrite it
        File.WriteAllText(filePath, playerDataToSave);
    }

    public void LoadFromJson()
    {
        // Since we want to set the playerData residing in the Player Component with the loaded data
        // we need access to it, we find the gameObject by Tag and retrieve the Component
        player = GameObject.FindWithTag(GameTags.PLAYER).GetComponent<Player>();
        
        // Very important!
        // Before actually loading, we check if the file exists, if not, we will get a FileNotFoundException
        // and the game will crash
        if (File.Exists(filePath))
        {
            // With the System.IO class File we can utilize the .ReadAllText method in order to read the content from a text file
            // Since the content of a text file is pure text, we store that in a local string variable
            string loadedPlayerData = File.ReadAllText(filePath);

            // We know that we want to load PlayerData, so we specify that and with the .FromJson method we translate the
            // content of the text back to their original form with the content of the json formatted text file which we previously
            // stored in the local string
            player.Data = JsonUtility.FromJson<PlayerData>(loadedPlayerData);
        }
        else
        {
            // If no file is present, we just print a log
            Debug.Log("No PlayerData Savefile found.");
        }
    }
}