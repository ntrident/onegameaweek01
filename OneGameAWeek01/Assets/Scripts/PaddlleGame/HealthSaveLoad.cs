using UnityEngine;

/// <summary>
/// A static class for quick access to save loading operations
/// </summary>
public static class HealthSaveLoad
{
    // A private string variable used as an Identifier/Key, needs to be static since the class is static
    private static string HEALTH_KEY = "HEALTH";

    /// <summary>
    /// Saves the health
    /// Static method which uses PlayerPrefs to save the parameter to disk
    /// </summary>
    /// <param name="playerHealth"></param>
    public static void SaveHealth(int playerHealth)
    {
        // First we use the .SetInt() Method to tell the PlayerPrefs WHAT, in our case the provided health value,
        // to save it with an identifier/key
        PlayerPrefs.SetInt(HEALTH_KEY, playerHealth);
        
        // Then we call the .Save() Method to actually (over)write the PlayerPrefs file
        PlayerPrefs.Save();
    }

    /// <summary>
    /// Loads the health
    /// Static method which retrieves our health value from the PlayerPrefs
    /// </summary>
    /// <returns></returns>
    public static int LoadHealth()
    {
        // We use the .GetInt() Method to retrieve the health value by using the
        // identifiert/key we used for saving
        return PlayerPrefs.GetInt(HEALTH_KEY);
    }
}