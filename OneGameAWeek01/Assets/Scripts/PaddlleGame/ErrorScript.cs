using UnityEngine;
using Random = UnityEngine.Random;

public class ErrorScript : MonoBehaviour
{
    /// <summary>
    /// Public variable for our array of transform reference
    /// Populate these in the Unity Inspector!
    /// </summary>
    [SerializeField]
    private Transform[] otherTransforms;

    /// <summary>
    /// Public Vector3 variable to move our transform array elements to a specific direction
    /// </summary>
    [SerializeField]
    private Vector3 direction;

    /// <summary>
    /// Public float variable for more flexible movement
    /// </summary>
    [SerializeField]
    private float speed;

    private void Update()
    {
        // We cycle through our array and check the elements for null
        for (int i = 0; i < otherTransforms.Length; i++)
        {
            // We only allow movement if the elements are not null!
            if (otherTransforms[i] != null)
            {
                otherTransforms[i].position += direction * speed * Time.deltaTime;
            }
        }
    }

    /// <summary>
    /// Public Method we hook up in the OnClick-Handler of a Unity UI Button
    /// </summary>
    public void DestroyIt()
    {
        // A local int variable which is populated with a random value
        // This random value is min 0 and max the length of our array
        // See the Unity Documentation for the Random.Range method!
        int randomIndex = Random.Range(0, otherTransforms.Length);

        // Before we destroy the element, we check once again if it exists
        // We can't destroy something that is already destroyed!
        if (otherTransforms[randomIndex] != null)
        {
            // We destroy the GameObject in the array with our randomly chosen index
            Destroy(otherTransforms[randomIndex].gameObject);
        }
    }
}