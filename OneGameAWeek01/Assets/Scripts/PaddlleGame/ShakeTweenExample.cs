using DG.Tweening;
using UnityEngine;


public class ShakeTweenExample : MonoBehaviour
{
    /// <summary>
    /// Public variable for the desired shake strength, this is basicaly the amplitude in all axis
    /// </summary>
    public float strength;

    /// <summary>
    /// Public variable for the desired vibration amount
    /// </summary>
    public int vibrato;

    /// <summary>
    /// Public variable for the desired randomness factor of the shake
    /// </summary>
    public float randomness;

    /// <summary>
    /// Public variable for the desired duration of the tween
    /// </summary>
    public float tweenDuration;

    /// <summary>
    /// Public Method we hook up in the OnClick-Handler of a Unity UI Button
    /// </summary>
    public void PlayTween()
    {
        // We check if a tween is currently in the tweening process
        if (DOTween.IsTweening(transform))
        {
            // If it's the case we complete/finish the tween in order to correctly
            // return to the values we started with initially
            transform.DOComplete();
        }

        // With the .DOShakePosition DOTween method we can shake a transform
        // with our parameters
        transform.DOShakePosition(tweenDuration, strength, vibrato, randomness);
    }
}