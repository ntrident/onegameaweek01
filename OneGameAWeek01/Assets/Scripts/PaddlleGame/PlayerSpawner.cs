using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    /// <summary>
    /// The reference of the Prefab Asset we want to spawn
    /// </summary>
    public GameObject playerPrefab;
    
    
    /// <summary>
    /// A private variable where we store the actual spawned player at runtime
    /// </summary>
    private GameObject spawnedPlayer;

    /// <summary>
    /// The OnClick Callback Method where we spawn the player
    /// </summary>
    public void SpawnThePlayer()
    {
        // Since we only want to spawn one Player we check if the variable has a reference
        // if not, we spawn the player
        if (spawnedPlayer == null)
        {
            spawnedPlayer = Instantiate(playerPrefab);
        }
    }

    /// <summary>
    /// The OnClick Callback Method where we destroy the current spawned player
    /// </summary>
    public void DestroyThePlayer()
    {
        // We only destroy the player if they exist
        if (spawnedPlayer != null)
        {
            Destroy(spawnedPlayer);
        }
    }
}