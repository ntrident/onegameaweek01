using System;
using UnityEngine;

/// <summary>
/// Similar to [SerializeField] wu use the [Serializable]-Attribute
/// to serialize a class object itself
/// </summary>
[Serializable]
public class PlayerData
{
    // we declare the variables as public in order to be able to save them
    // with the JsonUtility
    
    public int     health;
    public string  name;
    public Vector3 position;
}