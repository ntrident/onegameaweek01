using UnityEngine;

public class CharacterControllerMovement : MonoBehaviour
{
    /// <summary>
    /// Serialized value of our movement speed, assign this in the Inspector
    /// Should be at least 1
    /// </summary>
    [SerializeField]
    private float moveSpeed;

    /// <summary>
    /// Serialized value of run speed, assign this in the Inspector
    /// Should be higher than moveSpeed
    /// </summary>
    [SerializeField]
    private float runSpeed;

    /// <summary>
    /// Serialized value our movement speed, assign this in the Inspector
    /// Use the gravity of the earth: -9.81
    /// </summary>
    [SerializeField]
    private float gravity;

    /// <summary>
    /// Internally stored CharacterController Component
    /// </summary>
    private CharacterController characterController;

    /// <summary>
    /// Vector3 we use to move the CharacterController
    /// The value is set in Update()
    /// </summary>
    private Vector3 motion;

    /// <summary>
    /// Internally stored speed value, we will use this variable to store the initial speed value of moveSpeed
    /// </summary>
    private float initialSpeed;
    
    private void Awake()
    {
        // Attach this script to a GameObject with a CharacterController-Component
        // Only then we can store the CharacterController component into this variable
        characterController = GetComponent<CharacterController>();
        
        // Storing the moveSpeed value, which we assigned in the Inspector, in the initialSpeed value for later use
        initialSpeed = moveSpeed;
    }

    private void Update()
    {
        // The Input.GetAxis() Methods give us the values of the respective axis
        // in our case we want to get the values of the Horizontal and Vertical Axis

        // "Horizontal" is the horizontal axis (AD Keys and Left/Right Arrows)- See Project Input Settings
        // "Vertical" is the vertical axis (WS Keys and Up/Down Arrows)- See Project Input Settings
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput   = Input.GetAxis("Vertical");

        
        // Checking if the left shift Key is pressed and hold, if that's the case
        // we assign the moveSpeed value to the runSpeed value
        // if we don't hold the left shift Key we just use the initial speed
        if (Input.GetKey(KeyCode.LeftShift))
        {
            moveSpeed = runSpeed;
        }
        else
        {
            moveSpeed = initialSpeed;
        }

        // We want to move into the direction of our input, and since we
        // get the input values of the axis every frame, we can now populate the Vector for our movement
        // x is set to the horizontalInput value which will move the characterController right or left
        // y is set to the gravity value which will pull the characterController down
        // z is set to the verticalInput value which will move the characterController forward or backward
        motion = new Vector3(horizontalInput, gravity, verticalInput);

        // We setup the Vector for movement, now we apply it to the .Move() method of the characterController
        // In order to stay framerate independent we multiply the vector by Time.deltaTime and also
        // by the moveSpeed in order to maintain control of speed
        characterController.Move(motion * moveSpeed * Time.deltaTime);
    }
}