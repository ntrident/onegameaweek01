using UnityEngine;

public class FindPlayerByName : MonoBehaviour
{
    /// <summary>
    /// The name of the player - Set this in the inspector
    /// </summary>
    public string playerName;
    
    /// <summary>
    /// The private reference to a GameObject called player
    /// </summary>
    private GameObject playerGO;
    
    
    private void Update()
    {
        // DONT DO THIS:
        FindPlayerHardcoded();
        
        // INSTEAD DO THIS
        FindNameByVariable();
    }

    /// <summary>
    /// Finds the GameObject via it's name
    /// The name value is hardcoded in this method
    /// </summary>
    public void FindPlayerHardcoded()
    {
        if (playerGO == null)
        {
            // DONT HARDCODE STRINGS
            playerGO = GameObject.Find("Player");
        }
    }

    /// <summary>
    /// Finds the GameObject via it's name
    /// The name is set in a variable we set in the inspector
    /// </summary>
    public void FindNameByVariable()
    {
        if (playerGO == null)
        {
            // USE VARIABLES INSTEAD
            playerGO = GameObject.Find(playerName);
        }
    }
}