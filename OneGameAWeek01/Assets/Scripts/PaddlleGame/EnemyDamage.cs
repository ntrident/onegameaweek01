using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    /// <summary>
    /// Serialized value for the damage we will receive by an Enemy, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private int damage;

    /// <summary>
    /// Public Property of the private variable damage, we use properties to redirect
    /// to a private variable
    ///
    /// This property has only a getter because we do not want to set the value from somewhere else
    /// </summary>
    public int Damage
    {
        get { return damage; }
    }
}