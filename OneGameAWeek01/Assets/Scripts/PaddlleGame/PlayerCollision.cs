using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    /// <summary>
    /// Serialized value for our health, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private int health;

    /// <summary>
    /// Internal value for our enemy tag we setup in the Unity TagManager
    /// </summary>
    private string enemyTag = "Enemy";


    /// <summary>
    /// Special Unity Message like Awake or Update
    /// OnTriggerEnter is called only when:
    /// 1) We have any kind of Collider attached
    /// 2) The other GameObject we collide with has any kind of Collider attached
    /// 3) The other Gameobject has also a Rigidbody Component
    /// 4) The collider of the other GameObject is marked as "Is Trigger"
    ///
    /// This method will be called only once upon first entering the other collider
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        // With the .CompareTag method we can compare tags more efficiently than with ==
        // We check for the tag since we only want to be damaged by GameObejcts which are tagged as an Enemy
        if (other.CompareTag(enemyTag))
        {
            Debug.Log("Damage Player!");

            // The Collider argument has all the information we need
            // We know that Collider is a component and components are only attached to GameObjects
            // And only GameObjects can have components, which implies that we can safely use the GetComponent Method
            // to look for the EnemyDamage class in order to damage the Player
            health -= other.GetComponent<EnemyDamage>().Damage;
        }
    }

    /// <summary>
    /// Special Unity Message like Awake or Update
    /// OnTriggerEnter is called only when:
    /// 1) We have any kind of Collider attached
    /// 2) The other GameObject we collide with has any kind of Collider attached
    /// 3) The other Gameobject has also a Rigidbody Component
    /// 4) The collider of the other GameObject is marked as "Is Trigger"
    ///
    /// This method will be called all the time, while our collider is overlapping/inside the other collider
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        Debug.Log("Staying in Trigger");
    }


    /// <summary>
    /// Special Unity Message like Awake or Update
    /// OnTriggerEnter is called only when:
    /// 1) We have any kind of Collider attached
    /// 2) The other GameObject we collide with has any kind of Collider attached
    /// 3) The other Gameobject has also a Rigidbody Component
    /// 4) The collider of the other GameObject is marked as "Is Trigger"
    ///
    /// This method will be called once our collider exits the other collider
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Exiting Trigger");
    }
}