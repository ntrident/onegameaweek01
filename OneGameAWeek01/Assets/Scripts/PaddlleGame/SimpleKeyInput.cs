using UnityEngine;

public class SimpleKeyInput : MonoBehaviour
{
    /// <summary>
    /// Serialized value of the Keycode, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private KeyCode spaceKeyCode;
    
    private void Update()
    {
        // The Input class provides us with methods to check for specific Input
        // The .GetKey() Methods check if a very specific Key was pressed in a specific way
        
        
        // Checking if our provided key was initially pressed in this frame and is being held this frame
        if (Input.GetKey(spaceKeyCode))
        {
            Debug.Log("Space Input when pressed and hold this frame!");
        }

        // Checking if our provided key was pressed once in this frame
        if (Input.GetKeyDown(spaceKeyCode))
        {
            Debug.Log("Space Input only when pressed once this frame!");
        }

        // Checking if our provided key was released in this frame
        if (Input.GetKeyUp(spaceKeyCode))
        {
            Debug.Log("Space Input only when released this frame!");
        }
    }
}