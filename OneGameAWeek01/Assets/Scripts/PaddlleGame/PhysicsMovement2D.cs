using UnityEngine;

public class PhysicsMovement2D : MonoBehaviour
{
    /// <summary>
    /// Serialized value for our force vector, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private Vector2 force;

    /// <summary>
    /// Internally stored reference to the attached Rigidbody2D component
    /// </summary>
    private Rigidbody2D body2d;

    private void Awake()
    {
        // With GetComponent we get any component of the gameObject, in this case the Rigidbody2D
        body2d = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        // We only want to move the Rigidbody2D to the right, when the D-Key is hit
        if (Input.GetKey(KeyCode.D))
        {
            // With the .AddForce Method we impact the Rigidbody2D with a force, in this case our Vector2D
            body2d.AddForce(force);
        }
        // We only want to move the Rigidbody2D to the left, when the A-Key is hit
        else if (Input.GetKey(KeyCode.A))
        {
            // With the .AddForce Method we impact the Rigidbody2D with a force, in this case our Vector2D
            // We negate the Vector with a '-' in order to move the Rigidbody2D to the left
            body2d.AddForce(-force);
        }
    }
}