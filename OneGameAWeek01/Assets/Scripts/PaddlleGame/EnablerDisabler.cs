using UnityEngine;

public class EnablerDisabler : MonoBehaviour
{
    /// <summary>
    /// The name of the player
    /// </summary>
    public  string     name;
    
    /// <summary>
    /// Bool to set in the Inspector to set if the GameObject should be active or not
    /// </summary>
    public  bool       shouldGameObjectBeEnabled;
    
    /// <summary>
    /// The reference to the specific GameObject, we find it at runtime
    /// </summary>
    private GameObject otherGO;

    private void Start()
    {
        // At startup we immediately find the GameObject by name with the GameObject.Find Method
        otherGO = GameObject.Find(name);
    }

    private void Update()
    {
        // In Update we set the found GameObject to be active or not, depending on the state
        // of our bool variable
        otherGO.SetActive(shouldGameObjectBeEnabled);
    }
}