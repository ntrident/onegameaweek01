using UnityEngine;

public class DirectMovement : MonoBehaviour
{
    /// <summary>
    /// Serialized value of the Direction Vector, set this in the Inspector
    /// Preferable to 1/0/0
    /// </summary>
    [SerializeField]
    private Vector3 direction;

    /// <summary>
    /// Serialized value of our movementspeed, should be at least 1
    /// </summary>
    [SerializeField]
    private float speed;

    /// <summary>
    /// Our A-Key on the keyboard
    /// </summary>
    private KeyCode aKey = KeyCode.A;


    /// <summary>
    /// Our D-Key on the keyboard
    /// </summary>
    private KeyCode dKey = KeyCode.D;

    private void Update()
    {
        // We only want to move the object to the left when the A-Key is held down
        if (Input.GetKey(aKey))
        {
            // In order to move we continuously change the position vector of our transform
            // in order to be framerate independent we have to multiply the direction by Time.deltaTime
            // This will kind of "slow" the movement down, so we introduce a speed variable at the top
            // and also multiply the direction vector with the speed
            // Since we want to translate the object to the left, we use the negative direction
            transform.position += -direction * speed * Time.deltaTime;
        }

        // We only want to move the object to the right when the D-Key is held down
        if (Input.GetKey(dKey))
        {
            // In order to move we continuously change the position vector of our transform
            // in order to be framerate independent we have to multiply the direction by Time.deltaTime
            // This will kind of "slow" the movement down, so we introduce a speed variable at the top
            // and also multiply the direction vector with the speed
            // Since we want to translate the object to the right, we use the negative direction
            transform.position += direction * speed * Time.deltaTime;
        }
    }
}