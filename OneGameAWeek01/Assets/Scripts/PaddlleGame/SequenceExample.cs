using DG.Tweening;
using UnityEngine;

public class SequenceExample : MonoBehaviour
{
    /// <summary>
    /// Public variable for the desired X-axis position of the tween
    /// </summary>
    public float endPositionX;

    /// <summary>
    /// Public variable for the desired Y-axis position of the tween
    /// </summary>
    public float endPositionY;

    /// <summary>
    /// Public variable for the desired X-axis scale of the tween
    /// </summary>
    public float endScaleX;

    /// <summary>
    /// Public variable for the desired Y-axis scale of the tween
    /// </summary>
    public float endScaleY;

    /// <summary>
    /// Public variable for the desired duration of the tween
    /// </summary>
    public float duration;

    public void PlaySequence()
    {
        // In order to play a sequence we have to create one, the sequence will start
        // playing immediately
        Sequence customSequence = DOTween.Sequence();

        // We append our first tween into the internal list of tweens of a sequence
        // the first tween is the DOLocalMoveX Tween with the desired variables as parameters
        customSequence.Append(transform.DOLocalMoveX(endPositionX, duration));

        // We append our second tween into the internal list of tweens of a sequence
        // the second tween is the DOLocalMoveY Tween with the desired variables as parameters
        customSequence.Append(transform.DOLocalMoveY(endPositionY, duration));

        // We append our third tween into the internal list of tweens of a sequence
        // the third tween is the DOScaleX Tween with the desired variables as parameters
        customSequence.Append(transform.DOScaleX(endScaleX, duration));

        // We append our fourth tween into the internal list of tweens of a sequence
        // the fourth tween is the DOScaleY Tween with the desired variables as parameters
        customSequence.Append(transform.DOScaleY(endScaleY, duration));
    }
}