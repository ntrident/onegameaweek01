using TMPro;
using UnityEngine;

public class SimpleAxisInput : MonoBehaviour
{
    /// <summary>
    /// Serialized value of the TextMeshProUGUI Component, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI horizontalTextMesh;


    /// <summary>
    /// Serialized value of the TextMeshProUGUI Component, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI verticalTextMesh;

    private void Update()
    {
        // The Input.GetAxis() Methods give us the values of the respective axis
        // in our case we want to get the values of the Mouse-Axis

        // "Mouse X" is the horizontal axis of the mouse - See Project Input Settings
        // "Mouse Y" is the vertical axis of the mouse - See Project Input Settings
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        // Once we get the axis values we set the text properties of our textMeshes to those values
        horizontalTextMesh.text = mouseX.ToString();
        verticalTextMesh.text   = mouseY.ToString();
    }
}