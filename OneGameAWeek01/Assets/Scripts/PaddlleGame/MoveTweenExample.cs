using DG.Tweening;
using UnityEngine;

public class MoveTweenExample : MonoBehaviour
{
    /// <summary>
    /// Public variable for the desired endposition of the movement tween
    /// </summary>
    public float endPosition;

    /// <summary>
    /// Public variable for the desired delay for the start of the tween
    /// </summary>
    public float delay;

    /// <summary>
    /// Public variable for the desired amount of loops of a time,
    /// When set to -1 it will loop to infinity (and beyond!)
    /// </summary>
    public int loopAmount;

    /// <summary>
    /// Public variable/enum for the desired loop type of the tween
    /// </summary>
    public LoopType loopType;

    /// <summary>
    /// Public variable for the desired easing type of the tween
    /// </summary>
    public Ease easeType;

    /// <summary>
    /// Public variable for the desired duration of the tween
    /// </summary>
    public float tweenDuration;

    /// <summary>
    /// Public Method we hook up in the OnClick-Handler of a Unity UI Button
    /// </summary>
    public void PlayTween()
    {
        // With the .DOLocalMoveX DOTween method we can move a transform only along the X-axis with our parameters
        transform.DOLocalMoveX(endPosition, tweenDuration)
                 .SetDelay(delay)                // the .SetDelay Method on the tween object can delay the start of the tween
                 .SetEase(easeType)              // the .SetEase Method on the tween object can set the ease type of the tween
                 .SetLoops(loopAmount, loopType) // the .SetLoops Method on the tween object can set the amount of repetitions of a tween
                 .OnStart(MoveStarted)           // the .OnStart Method invokes a specific Method at the start of the tween
                 .OnComplete(MoveFinished);      // the .OnComplete Method invokes a specific Method when the tween is finished
    }

    /// <summary>
    /// Callback Method when our tween is started
    /// </summary>
    private void MoveStarted()
    {
        Debug.Log("MoveTween is started!");
    }

    /// <summary>
    /// Callback Method when our tween is finished
    /// </summary>
    private void MoveFinished()
    {
        Debug.Log("MoveTween is finished!");
    }
}