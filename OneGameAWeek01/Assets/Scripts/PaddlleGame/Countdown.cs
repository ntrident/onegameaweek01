using TMPro;
using UnityEngine;

public class Countdown : MonoBehaviour
{
    /// <summary>
    /// Serialized value of the countdown, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private float countdown;

    /// <summary>
    /// Variable in order to store the value of the countdown variable at Startup
    /// </summary>
    private float initialCountdown;


    /// <summary>
    /// Serialized value of the targeted TextMeshProUGUI component, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI textMesh;


    /// <summary>
    /// Our internal countdown which is used to pause between the "real" countdown
    /// </summary>
    private float internalCountdown = 3f;

    private void Awake()
    {
        // At startup we store the value of countdown to initialCountdown to be able
        // to work with this initial value later
        initialCountdown = countdown;
    }

    private void Update()
    {
        // Time.deltaTime gives us the real-time elapsed time between the last and current frame
        // So we use that value to count down
        countdown -= Time.deltaTime;
        if (countdown <= 0f)
        {
            // When we reach 0, we stay at 0
            countdown = 0f;

            // At this point we start to count down our other internal countdown
            internalCountdown -= Time.deltaTime;
            if (internalCountdown <= 0f)
            {
                // When this internal countdown reaches 0 we reset the real countdown to its initial value
                // which we stored in the initialCountdown variable in Awake()
                countdown = initialCountdown;

                // We also reset the internal countdown
                internalCountdown = 3f;
            }
        }

        // At the end we set the text property of the textMesh to the value of the real countdown
        // In order to display the float variable we use the .ToString() method to format the float as a string
        textMesh.text = countdown.ToString();
    }
}