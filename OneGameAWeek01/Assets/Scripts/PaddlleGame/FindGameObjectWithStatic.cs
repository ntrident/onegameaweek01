using UnityEngine;

public class FindGameObjectWithStatic : MonoBehaviour
{
    /// <summary>
    /// GameObject Reference we find through our static class
    /// DO NOT SET THIS IN THE INSPECTOR!
    /// </summary>
    [SerializeField]
    private GameObject gameObjectToFind;

    private void Start()
    {
        // Using our static class and its static Method to find a GameObject
        gameObjectToFind = GameTags.FindPlayer();
    }
}