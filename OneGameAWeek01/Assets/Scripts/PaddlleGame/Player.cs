using UnityEngine;

public class Player : MonoBehaviour
{
    // Since this is a private field we use [SerializeField]
    // to expose it to the Unity Inspector
    /// <summary>
    /// A private instance of the PlayerData class,
    /// initialised in the Unity Inspector
    /// </summary>
    [SerializeField]
    private PlayerData playerData;

    /// <summary>
    /// Property of our private playerData-variable
    /// </summary>
    public PlayerData Data
    {
        get { return playerData; }
        set { playerData = value; }
    }

    private void Start()
    {
        // At start we set the position of our transform component
        // to the Vector3 variable we set in the position variable
        // of our playerData instance
        // We set the value of the .position variable by loading
        // it at Awake in the SaveLoadByJson Script, check it out.
        transform.localPosition = playerData.position;
    }
}