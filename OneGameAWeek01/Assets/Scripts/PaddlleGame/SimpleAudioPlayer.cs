using UnityEngine;

public class SimpleAudioPlayer : MonoBehaviour
{
    /// <summary>
    /// Serialized value for our AudioClip we want to play, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private AudioClip clip;

    /// <summary>
    /// Internally stored reference to the attached AudioSource component
    /// </summary>
    private AudioSource audioSource;

    private void Awake()
    {
        // With GetComponent we get any component of the gameObject, in this case the AudioSource
        audioSource = GetComponent<AudioSource>();

        // We assign our clip to the clip value of the audioSource reference
        audioSource.clip = clip;
    }

    private void Start()
    {
        // with .Play() we play the clip inside the audioSource, in Awake() we did assign a clip, so this will work correctly
        audioSource.Play();
    }
}