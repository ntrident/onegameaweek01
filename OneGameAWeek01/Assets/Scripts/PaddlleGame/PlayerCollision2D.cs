using UnityEngine;

public class PlayerCollision2D : MonoBehaviour
{
    /// <summary>
    /// Serialized value for our health, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private int health;

    /// <summary>
    /// Internal value for our enemy tag we setup in the Unity TagManager
    /// </summary>
    private string enemyTag = "Enemy";


    private void Start()
    {
        // At bootup we immediately load the saved health value by calling the static class and its method
        // If there was never a save-operation, we will get 0 back in our current case
        // which will set health = 0
        health = HealthSaveLoad.LoadHealth();
    }

    /// <summary>
    /// Special Unity Message like Awake or Update
    /// OnTriggerEnter2D is called only when:
    /// 1) We have any kind of Collider2D attached
    /// 2) The other GameObject we collide with has any kind of Collider2D attached
    /// 3) The other Gameobject has also a Rigidbody2D Component
    /// 4) The Collider2D of the other GameObject is marked as "Is Trigger"
    ///
    /// This method will be called only once upon first entering the other Collider2D
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        // With the .CompareTag method we can compare tags more efficiently than with ==
        // We check for the tag since we only want to be damaged by GameObejcts which are tagged as an Enemy
        if (other.CompareTag(enemyTag))
        {
            Debug.Log("Damage Player!");

            // The Collider2D argument has all the information we need
            // We know that Collider2D is a component and components are only attached to GameObjects
            // And only GameObjects can have components, which implies that we can safely use the GetComponent Method
            // to look for the EnemyDamage class in order to damage the Player
            health -= other.GetComponent<EnemyDamage>().Damage;
            
            // After getting damaged we instantly save the value by calling the static method of our static class
            // and pass in the health variable
            HealthSaveLoad.SaveHealth(health);
        }
    }


    /// <summary>
    /// Special Unity Message like Awake or Update
    /// OnTriggerStay2D is called only when:
    /// 1) We have any kind of Collider2D attached
    /// 2) The other GameObject we collide with has any kind of Collider2D attached
    /// 3) The other GameObject has also a Rigidbody2D Component
    /// 4) The Collider2D of the other GameObject is marked as "Is Trigger"
    ///
    /// This method will be called all the time, while our Collider2D is overlapping/inside the other Collider2D
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay2D(Collider2D other)
    {
        Debug.Log("Staying in Trigger");
    }


    /// <summary>
    /// Special Unity Message like Awake or Update
    /// OnTriggerExit2D is called only when:
    /// 1) We have any kind of Collider2D attached
    /// 2) The other GameObject we collide with has any kind of Collider2D attached
    /// 3) The other GameObject has also a Rigidbody2D Component
    /// 4) The Collider2D of the other GameObject is marked as "Is Trigger"
    ///
    /// This method will be called once our Collider2D exits the other Collider2D
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log("Exiting Trigger");
    }
}