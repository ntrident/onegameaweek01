using UnityEngine;

public class ElegantPlayerSpriteChanger : MonoBehaviour
{
    /// <summary>
    /// The public reference to the SpriteRenderer of the Player GameObject
    /// </summary>
    public SpriteRenderer playerSpriteRenderer;
    
    /// <summary>
    /// The OnClick-Callback Method set in the Button-OnClick List
    /// The Sprite parameter is what we want our PlayerSpriteRenderer-Sprite to change into
    /// </summary>
    /// <param name="newPlayerSprite"></param>
    public void ChangeToSprite(Sprite newPlayerSprite)
    {
        playerSpriteRenderer.sprite = newPlayerSprite;
    }
}