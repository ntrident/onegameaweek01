using UnityEngine;


public class SimpleButtonInput : MonoBehaviour
{
    /// <summary>
    /// Serialized value of the Keycode, assign this in the Inspector
    /// Make sure this value has the same name as the Jump-Action in the Project Input Settings
    /// </summary>
    [SerializeField]
    private string jumpAction;

    private void Update()
    {
        // The Input class provides us with methods to check for specific Input
        // The .GetButton() Methods check if the Input-Action was executed in a specific way

        // Checking if our provided action was initially performed in this frame and is being held this frame
        if (Input.GetButton(jumpAction))
        {
            Debug.Log("Jump Action was pressed and is being held this frame!");
        }

        // Checking if our provided action was initially performed in this frame
        if (Input.GetButtonDown(jumpAction))
        {
            Debug.Log("Jump Action was pressed once this frame!");
        }

        // Checking if our provided action was released in this frame
        if (Input.GetButtonUp(jumpAction))
        {
            Debug.Log("Jump Action was released this frame!");
        }
    }
}