using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    /// <summary>
    /// Serialized value of our mouse speed, assign this in the Inspector
    /// Should be at least 1
    /// </summary>
    [SerializeField]
    private float mouseSensitivity;

    /// <summary>
    /// Internally stored reference to the Transform component of the Player
    /// Make sure that the Player is the parent of the Camera
    /// </summary>
    private Transform playerTransform;

    /// <summary>
    /// Internally stored value for our pitch axis rotation
    /// </summary>
    private float pitch;

    private void Start()
    {
        // Since we know the Player is our parent, we use transform.parent
        playerTransform = transform.parent;
    }

    private void Update()
    {
        // The Input.GetAxis() Methods give us the values of the respective axis
        // in our case we want to get the values of the Mouse-Axis

        // "Mouse X" is the horizontal axis of the mouse - See Project Input Settings
        // "Mouse Y" is the vertical axis of the mouse - See Project Input Settings

        // In order to have more control for the mouse we multiply the value we get with our variable
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

        // Since we want the camera to look up when the mouse goes to the top and the camera to look down 
        // when the mouse goes to the bottom, we substract the pitch by mouseY

        pitch -= mouseY;

        // We don't want to overshoot the value! Meaning, we don't want to rotate to infinity
        // The head on our body can't look beyond a specific angle, so we define a range from a min to a max
        // With the Mathf.Clamp Method
        pitch = Mathf.Clamp(pitch, -45f, 45f);

        // We rotate the transform of the camera
        // Rotations are represented by Quaternions in Unity
        // With Quaternion.Euler Method we can feed basically "degrees" either with a Vector3 or three floats functioning each
        // as a degree, the method gives a newly represented Quaternion back so Unity is able to translate this 
        // into the correct rotation
        transform.localRotation = Quaternion.Euler(pitch, 0f, 0f);

        // At the end we rotate the playerTransform based on the mouseX Input with the handy .Rotate method
        playerTransform.Rotate(0f, mouseX, 0f);
    }
}