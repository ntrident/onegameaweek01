using TMPro;
using UnityEngine;

public class FancyTimer : MonoBehaviour
{
    /// <summary>
    /// Serialized value of the timer, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private float timer;

    /// <summary>
    /// Serialized value of the TextMeshProUGUI component, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI singleDigitTextMesh;

    /// <summary>
    /// Serialized value of the TextMeshProUGUI component, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI doubleDigitTextMesh;


    /// <summary>
    /// Serialized value of the TextMeshProUGUI component, assign this in the Inspector
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI hourDisplayTextMesh;

    private void Update()
    {
        // Time.deltaTime gives us the real-time elapsed time between the last and current frame
        // So we use that value to count down
        timer -= Time.deltaTime;

        // Since floating values give us decimal values like 1.52834 this doesn't look that nice ingame
        // with the string.Format() method we're able to set the format of a value

        // This formatting setting will format the timer into a single digit like this: 5
        singleDigitTextMesh.text = string.Format("{0:0}", timer);

        // This formatting setting will format the timer into a number with two digits after the coma
        // like this: 5.74
        doubleDigitTextMesh.text = string.Format("{0:0.00}", timer);

        // This formatting setting will format the timer into a display like a digital clock
        // but it won't function like a clock!
        hourDisplayTextMesh.text = string.Format("{0:00:00:00}", timer);
    }
}