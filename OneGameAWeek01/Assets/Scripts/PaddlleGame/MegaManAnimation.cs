using System;
using UnityEngine;


public class MegaManAnimation : MonoBehaviour
{
    private Animator megaManAnimator;

    /// <summary>
    /// Serialized value for the walk state of the Animator we want to play
    /// </summary>
    [SerializeField]
    private string walkState;
    
    /// <summary>
    /// Serialized value for the idle state of the Animator we want to play
    /// </summary>
    [SerializeField] 
    private string idleState;
    
    private void Start()
    {
        // This script will be always attached to the GameObject with the Animator Component
        // so we can safely use GetComponent to retrieve the reference to the Animator
        megaManAnimator = GetComponent<Animator>();
    }
    private void Update()
    {
        // We check if either 'A' or 'D' on the keyboard is pressed and hold
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            // If it's the case, we play the walk state of the Animator via the walkState variable
            megaManAnimator.Play(walkState);
        }
        else
        {
            // If it's not the case we play the idle state of the Animator via the idleState variable
            megaManAnimator.Play(idleState);
        }
    }
}