using System.IO;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Minigame
{
    public class HighScore : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI textMesh;
        [SerializeField]
        private Vector3 punch;
        private int currentHighScore;


        private void Update()
        {
            textMesh.text = currentHighScore.ToString();
        }

        public void AddHighScore(int score)
        {
            currentHighScore += score;

            if (DOTween.IsTweening(transform))
            {
                transform.DOComplete();
            }

            transform.DOPunchScale(punch, 0.25f);
        }

        public void SaveHighScore()
        {
            string filePath = Application.persistentDataPath + "/HighScore.json";

            HighScoreData highScoreData = new HighScoreData();

            highScoreData.highScore = currentHighScore;

            string dataToSave = JsonUtility.ToJson(highScoreData);

            File.WriteAllText(filePath, dataToSave);
        }
    }
}