using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Minigame
{
    public class SceneLoader : MonoBehaviour
    {
        /// <summary>
        /// Serialized value of the name of the scene we want to load, assign in the Inspector
        /// </summary>
        [SerializeField]
        private string sceneToLoad;

        /// <summary>
        /// Internally stored reference to the Button component
        /// </summary>
        private Button button;

        private void Awake()
        {
            // Attach this script to a GameObject with a Button-Component
            // Only then we can store the Button component into this variable
            button = GetComponent<Button>();
        }

        private void OnEnable()
        {
            // Instead of using the OnClick Handler in the Inspector of the Button Component we
            // accomplish the very same behaviour via code using the onClick Event and its AddListener method
            // We register the Button-Click to the LoadScene() Method
            button.onClick.AddListener(LoadScene);
        }

        private void OnDisable()
        {
            // If you register to events in code you also have to "unregister" or "unsubscribe" or
            // If you do not this may lead to MemoryLeak!
            button.onClick.RemoveListener(LoadScene);
        }

        private void LoadScene()
        {
            // When we click the button, we load the desired scene by name
            SceneManager.LoadScene(sceneToLoad);
        }
    }
}