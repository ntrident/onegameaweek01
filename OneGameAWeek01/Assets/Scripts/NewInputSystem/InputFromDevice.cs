﻿using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;

namespace _01_InputSystem
{
    public class InputFromDevice : MonoBehaviour
    {
        [SerializeField]
        private float speed;

        [SerializeField]
        private Vector3 direction;

        [SerializeField]
        private bool useGamePad;

        private void Update()
        {
            if (useGamePad)
            {
                MoveDirectlyByGamePadInput();
            }
            else
            {
                MoveDirectlyByKeyboardInput();
            }
        }

        private void MoveDirectlyByKeyboardInput()
        {
            Keyboard myKeyboard = Keyboard.current;

            if (myKeyboard.aKey.isPressed)
            {
                transform.Translate(-direction.normalized * (speed * Time.deltaTime));
            }
            if (myKeyboard.dKey.isPressed)
            {
                transform.Translate(direction.normalized * (speed * Time.deltaTime));
            }
        }

        private void MoveDirectlyByGamePadInput()
        {
            Gamepad myGamepad = Gamepad.current;

            if (myGamepad.squareButton.isPressed)
            {
                transform.Translate(-direction.normalized * (speed * Time.deltaTime));
            }
            if (myGamepad.circleButton.isPressed)
            {
                transform.Translate(direction.normalized * (speed * Time.deltaTime));
            }
        }
    }
}
