using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace _01_InputSystem
{
    public class InputSideBySide : MonoBehaviour
    {
        private void Update()
        {
            ExecuteNewInput();
        }

        private void ExecuteOldInput()
        {
            if (Input.GetKey(KeyCode.A))
            {
                Debug.Log("Pressing the A Key on my keyboard, yey!");
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                Debug.Log("Pressing the A Key down once on my keyboard, yey!");
            }

            if (Input.GetKeyUp(KeyCode.A))
            {
                Debug.Log("Lifting my finger from the A Key on my keyboard, yey!");
            }
        }

        private void ExecuteNewInput()
        {
            Keyboard myKeyboard = Keyboard.current;

            if (myKeyboard.aKey.isPressed)
            {
                Debug.Log("Pressing the A Key on my keyboard with the new Input System, yey!");
            }

            if (myKeyboard.aKey.wasPressedThisFrame)
            {
                Debug.Log("Pressing the A Key down once on my keyboard with the new Input System, yey!");
            }

            if (myKeyboard.aKey.wasReleasedThisFrame)
            {
                Debug.Log("Lifting my finger from the A Key on my keyboard with the new Input System, yey!");
            }
        }
    }
}
