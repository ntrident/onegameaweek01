using System;

namespace Minigame
{
    /// <summary>
    /// Data Class to store and actually save the highScore to disk
    /// This class absolutely needs to have the [Serializable]-Attribute and its contents
    /// HAVE to be public, otherwise they won't get serialized/written to disk!
    /// </summary>
    [Serializable]
    public class HighScoreData
    {
        public int highScore;
    }
}