using System.IO;
using TMPro;
using UnityEngine;

namespace Minigame
{
    public class ShowHighScore : MonoBehaviour
    {
        /// <summary>
        /// Serialized value for a string text to display that there is no highscore, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private string noHighScoreMessage;

        /// <summary>
        /// Serialized reference for a TextMeshProUGUI Component which shall display the HighScore, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private TextMeshProUGUI highScoreTextMesh;

        /// <summary>
        /// Internally stored value for our loaded highScore
        /// </summary>
        private int loadedHighScore;

        /// <summary>
        /// Internally stored value to check if a HighScore SaveFile exists
        /// </summary>
        private bool doesHighScoreDataExist;

        /// <summary>
        /// Internally stored reference to the HighScoreData which should be loaded
        /// </summary>
        private HighScoreData highScoreData;

        /// <summary>
        /// Internally stored value for the HighScore SaveFile location
        /// </summary>
        private string filePath;

        private void Awake()
        {
            // The string property .persistentDataPath in the Application class of Unity points
            // to a very specific folder, a directory path where you can store data that you want to be kept between runs
            // It's a different location for each platform, please lookup the Scripting Manual
            // Since we create a file or overwrite an existing file when saving we have to define the filePath
            // We combine the persistentDataPath, effectively the folder where the savefile will be, with a custom file, consisting
            // of a name and the filetype - since we will use the JsonUtility for saving and loading, it's wise to set .json as the
            // filetype
            filePath = Application.persistentDataPath + "/HighScore.json";

            // At startup we load the HighScore, since the LoadHighScore() Method has the bool return type we store the bool value
            // we get back in our doesHighScoreDataExist value so we can work with that later on
            doesHighScoreDataExist = LoadHighScore();
        }

        private void Update()
        {
            // We check our doesHighScoreDataExist value and depending on that we display different things
            if (doesHighScoreDataExist)
            {
                // If doesHighScoreDataExist is true, we know the HighScore file was loaded successfully
                // and we can display the actual loaded highScore
                highScoreTextMesh.text = loadedHighScore.ToString();
            }
            else
            {
                // If doesHighScoreDataExist is false, we know the HighScore file does not exist and could not be loaded
                // so we display a specific message stating the fact that there is no HighScore yet
                highScoreTextMesh.text = noHighScoreMessage;
            }
        }

        /// <summary>
        /// Loads the HighScore File
        /// </summary>
        /// <returns></returns>
        private bool LoadHighScore()
        {
            // Very important!
            // Before actually loading, we check if the file exists, if not, we will get a FileNotFoundException
            // and the game will crash
            if (File.Exists(filePath))
            {
                // With the System.IO class File we can utilize the .ReadAllText method in order to
                // read the content from a text file
                // Since the content of a text file is pure text, we store that in a local string variable
                string loadedHighScoreData = File.ReadAllText(filePath);


                // We want to load the previously saved HighScoreData, so we specify that and with the .FromJson method we translate the
                // content of the text back to their original form with the content of
                // the json formatted text file which we previously stored in the local string
                highScoreData = JsonUtility.FromJson<HighScoreData>(loadedHighScoreData);

                // Since we loaded the data into our highScoreData variable we can now assign its stored .highScore value
                // to our loadedHighScore variable
                loadedHighScore = highScoreData.highScore;

                // We return true because we successfully loaded the savefile
                return true;
            }

            // If the file does not exist we return false, nothing got loaded
            return false;
        }
    }
}