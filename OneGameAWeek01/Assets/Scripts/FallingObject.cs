using UnityEngine;
using Random = UnityEngine.Random;

namespace Minigame
{
    public class FallingObject : MonoBehaviour
    {
        /// <summary>
        /// Serialized value of the min speed the falling object should have, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float minSpeed;

        /// <summary>
        /// Serialized value of the max speed the falling object may have, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float maxSpeed;

        /// <summary>
        /// Serialized value of the score we add to the Highscore when colliding with the playerpeddle
        /// assign this in the Inspector
        /// </summary>
        [SerializeField]
        private int score;

        /// <summary>
        /// Internally stored value for the movement speed
        /// </summary>
        private float speed;

        private void Awake()
        {
            // When a falling object gets spawned we set the speed value randomly between the
            // minSpeed and maxSpeed values
            speed = Random.Range(minSpeed, maxSpeed);
        }

        private void Update()
        {
            // The falling object goes straight down depending on its' speed value
            transform.position += Vector3.down * speed * Time.deltaTime;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            // Whatever we collide with, first we find the FallingObjectManager with its tag
            FallingObjectManager manager = GameObject.FindWithTag(GameTags.FALLINGOBJECTMANAGER).GetComponent<FallingObjectManager>();

            // When we collide with something that has the PLAYER Tag
            if (other.CompareTag(GameTags.PLAYER))
            {
                // we call the ProcessFallingObject() method and pass in the respective arguments
                // We mark this gameObject for destruction and allow to raise the highscore and add in the score of this falling object
                manager.ProcessFallingObject(gameObject, true, score);
            }

            // When we collide with something that has the DEATHZONE Tag
            if (other.CompareTag(GameTags.DEATHZONE))
            {
                // we call the ProcessFallingObject() method and pass in the respective arguments
                // We mark this gameObject for destruction and do not allow to raise the highscore, but we still add in the score
                // of this falling object because the third argument is necessary (generally speaking this is bad code design!)
                manager.ProcessFallingObject(gameObject, false, score);
            }
        }
    }
}