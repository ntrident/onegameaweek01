using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Minigame
{
    public class PlayerPaddleMovement : MonoBehaviour
    {
        [SerializeField]
        private bool useGamePad;

        [SerializeField]
        private Vector3 direction;

        /// <summary>
        /// Serialized value of the movement speed, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float speed;

        /// <summary>
        /// Serialized value of the left edge, this is the furthest the player may move to the left
        /// assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float leftEdge;

        /// <summary>
        /// Serialized value of the right edge, this is the furthest the player may move to the right
        /// assign this in the Inspector
        /// </summary>
        [SerializeField]
        private float rightEdge;

        /// <summary>
        /// Serialized value of a Color, used for a tween, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private Color tweenColor;

        /// <summary>
        /// Internally stored value of the initial Color of our SpriteRenderer
        /// </summary>
        private Color initialColor;

        /// <summary>
        /// Internally stored reference of the SpriteRenderer
        /// </summary>
        //private SpriteRenderer renderer;

        private void Awake()
        {
            // Wherever the player is in the scene, at startup we put it into a very specific position
            transform.position = new Vector3(0f, -4.5f);

            // Make sure the GameObject with this script has the SpriteRenderer Component
            // Only then we can store the SpriteRenderer component into this variable
            //renderer = GetComponent<SpriteRenderer>();

            // We store the initial color of the spriteRenderer
            //initialColor = renderer.color;
        }

        private void Update()
        {
            if (useGamePad)
            {
                MoveDirectlyByGamePadInput();
            }
            else
            {
                MoveDirectlyByKeyboardInput();
            }

            // Since we do not want to go beyond the scene/camera bounds, we force the position on the x-axis of our player
            // We accomplish this by clamping the position:

            // Transform.position is a so called "Struct" (Google that!)
            // And C# has a very specific way of accessing and handling these structs
            // We can't set the position.x property , we have to work with a temporary variable as a workaround
            // So we cache the current position
            Vector3 cachedPosition = transform.position;

            // Then we perform the clamping operation on the .x value of the cached Position
            // with the Mathf.Clamp() Method and pass in the value which has to be clamped, in our case
            // the cachedPosition.x, we clamp it between the leftEdge and the rightEdge
            // the value which gets returned will be set to the cachedPosition.x value
            cachedPosition.x = Mathf.Clamp(cachedPosition.x, leftEdge, rightEdge);

            // At last we set the transform.position to the cachedPosition Vector with it's clamped x-Value
            transform.position = cachedPosition;
        }

        private void MoveDirectlyByKeyboardInput()
        {
            Keyboard myKeyboard = Keyboard.current;

            if (myKeyboard.aKey.isPressed)
            {
                transform.Translate(-direction.normalized * (speed * Time.deltaTime));
            }
            if (myKeyboard.dKey.isPressed)
            {
                transform.Translate(direction.normalized * (speed * Time.deltaTime));
            }
        }

        private void MoveDirectlyByGamePadInput()
        {
            Gamepad myGamepad = Gamepad.current;

            if (myGamepad.squareButton.isPressed)
            {
                transform.Translate(-direction.normalized * (speed * Time.deltaTime));
            }
            if (myGamepad.circleButton.isPressed)
            {
                transform.Translate(direction.normalized * (speed * Time.deltaTime));
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            // We check if a tween is currently playing on the renderer, if so, we complete it to prevent faulty tween behaviour
            //if (DOTween.IsTweening(renderer))
            //{
                //renderer.DOComplete();
            //}

            // We check if a tween is currently playing on the transform, if so, we complete it to prevent faulty tween behaviour
            if (DOTween.IsTweening(transform))
            {
                transform.DOComplete();
            }

            // We perform a little PunchScale Tween on the playerpeddle when we trigger with any other collider
            // and perform a little color tween as well
            // The Color-Tween will call the ResetColor method when the tween is completed, since we do not want
            // to stay in the color we tweened to
            Vector3 punch = new Vector3(1f, 1f, 1f);
            transform.DOPunchScale(punch, 0.25f);
            //renderer.DOColor(tweenColor, 0.25f).OnComplete(ResetColor);
        }

        void ResetColor()
        {
            // Resetting the color to the initialColor at startup
            //renderer.color = initialColor;
        }
    }
}